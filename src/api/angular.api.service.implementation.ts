/*
 * Copyright (c) Clawiz
 */

import {HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';


import {Observable} from "rxjs";
import {ApiResponse, ApiRequestMethod, ApiHttpResponseImplementation, ApiErrorType, ApiServiceImplementation, Loading } from "clawiz-core";
import {AngularClawiz} from "../interfaces";

export class AngularApiServiceImplementation extends ApiServiceImplementation {

  angularClawiz : AngularClawiz;

  set clawiz( clawiz : AngularClawiz ) {
    this.angularClawiz = clawiz;
  }

  get clawiz() : AngularClawiz {
    return this.angularClawiz;
  }

  private prepareParameters(params : any) : HttpParams  {
    let result = new HttpParams();
    if ( params != null ) {
      for (let key in params) {
        if ( params[key] != undefined ) {
          result = result.append(key,
            params[key] instanceof Object ? JSON.stringify(params[key]) : params[key]
          );
        }
      }
    }
    if ( this.sessionId != null ) {
      result = result.append('cwsessionid', this.sessionId);
    }
    return result;
  }

  protected makeCall(response : ApiResponse, loading : Loading) : Promise<ApiResponse> {

    let call   : Observable<any>;
    let params  = this.prepareParameters(response.request.params);

    if ( response.request.method == ApiRequestMethod.GET ) {

      call = this.clawiz.config.api.http.get(this.getRequestUrl(response.request), { params : params });

    } else if ( response.request.method == ApiRequestMethod.POST ) {

      let headers = new HttpHeaders();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      call        = this.clawiz.config.api.http.post(this.getRequestUrl(response.request), params, { headers:headers });
    }

    return call.toPromise()
      .then((body) => {
        // console.log('API RESPONSE BODY : \n' + JSON.stringify(body.json()));
        let json         = body;
        let httpResponse = new ApiHttpResponseImplementation();
        for(let key in json) {
          httpResponse[key] = json[key];
          response[key]     = json[key];
        }
        response.httpResponse        = httpResponse;

        response.success             = httpResponse.status === 'OK';
        response.message             = httpResponse.message;
        response.displayMessage      = httpResponse.displayMessage != null ? httpResponse.displayMessage : httpResponse.message;

        return Promise.resolve(response);
      })
      .catch((reason) => {
        if ( loading != null ) { loading.hide(); }

        response.success             = false;
        response.errorType           = ApiErrorType.NETWORK_ERROR;
        response.message             = reason.message;
        response.displayMessage      = this.getNetworkErrorMessage();

        return Promise.resolve(response);
      })


  }


}
