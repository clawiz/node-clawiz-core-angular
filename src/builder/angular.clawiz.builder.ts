/*
 * Copyright (c) ISOApplication 2017.
 */

import {ClawizBuilder} from "clawiz-core";
import {AngularClawizConfig, AngularClawiz} from "../interfaces";
import {AngularClawizImplementation} from "./angular.clawiz.implementation";
import {AngularClawizConfigImplementation} from "./angular.clawiz.config.implementation";

export class AngularClawizBuilder extends ClawizBuilder {

  static angularClawiz : AngularClawiz;

  public static get clawiz() : AngularClawiz {
    return AngularClawizBuilder.angularClawiz;
  }

  public static init(config : AngularClawizConfig) : Promise<AngularClawiz> {
    return ClawizBuilder.createClawizInstance(AngularClawizImplementation, AngularClawizConfigImplementation, config)
      .then((clawiz : AngularClawiz) => {
        AngularClawizBuilder.angularClawiz = clawiz;
        return Promise.resolve(clawiz);
      });
  }

}
