
/*
 * Copyright (c) ISOApplication 2017.
 */

import {ClawizConfigImplementation} from "clawiz-core";
import {
    AngularClawizConfig, AngularApiConfig, AngularUiConfig, AngularApplicationConfig,
    AngularStorageConfig
} from "../interfaces";

export class AngularClawizConfigImplementation extends ClawizConfigImplementation implements AngularClawizConfig {

  application : AngularApplicationConfig;
  storage     : AngularStorageConfig;
  api         : AngularApiConfig;
  ui          : AngularUiConfig;

}
