/*
 * Copyright (c) ISOApplication 2017.
 */

import {ClawizImplementation} from "clawiz-core";
import {AngularClawiz, AngularClawizConfig} from "../interfaces";
import {AngularApiServiceImplementation} from "../api/angular.api.service.implementation";
import {AngularUiServiceImplementation} from "../ui/angular.ui.service.implementation";
import {AbstractAngularRouterService} from "../ui/router/abstract.angular.router.service";

export class AngularClawizImplementation extends ClawizImplementation implements AngularClawiz {

  get config(): AngularClawizConfig {
    return <AngularClawizConfig>this._config;
  }

  private _angularUiService : AngularUiServiceImplementation;

  get ui(): AngularUiServiceImplementation {
    return this._angularUiService;
  }

  set ui(uiService: AngularUiServiceImplementation) {
    this._angularUiService = uiService;
  }

  protected getApiService(): Promise<AngularApiServiceImplementation> {
    return this.getService(AngularApiServiceImplementation);
  }


  protected getUiService(): Promise<AngularUiServiceImplementation> {
    return this.getService(AngularUiServiceImplementation);
  }


    protected getRouterService(): Promise<AbstractAngularRouterService> {
        return this.getService(AbstractAngularRouterService)
    }
}
