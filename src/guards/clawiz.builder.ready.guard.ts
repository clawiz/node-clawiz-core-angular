/*
 * Copyright (c) ISOApplication 2017.
 */


import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ClawizBuilder } from "clawiz-core";
import {AngularClawiz} from "../interfaces";

@Injectable()
export class ClawizBuilderReadyGuard implements CanActivate {

  public static CLAWIZ_BUILDER_READY_GUARD = 'ClawizBuilderReadyGuard';

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean {
    let clawiz : AngularClawiz = <AngularClawiz>ClawizBuilder.clawiz;

    if ( ClawizBuilder.ready ) {
      return true;
    }

    let startingPath =
      ( clawiz == undefined || clawiz.config == undefined || clawiz.config.ui == undefined || clawiz.config.ui.loginPath == undefined )
        ? '/starting' : clawiz.config.ui.startingPath;

    console.log('ClawizBuilder not ready');
    this.router.navigate([startingPath]);
    return false;
  }
}
