/*
 * Copyright (c) ISOApplication 2017.
 */


import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ClawizBuilder } from "clawiz-core";
import {AngularClawiz} from "../interfaces";

@Injectable()
export class UserLoggedCanActivateGuard implements CanActivate {

  public static USER_LOGGED_CAN_ACTIVATE_GUARD = 'UserLoggedCanActivateGuard';

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean {
    let clawiz : AngularClawiz = <AngularClawiz>ClawizBuilder.clawiz;

    if ( clawiz != undefined  && clawiz.node != undefined  && clawiz.node.userLogged ) {
      return true;
    }

    let loginPath =
      ( clawiz == undefined || clawiz.config == undefined || clawiz.config.ui == undefined || clawiz.config.ui.loginPath == undefined )
      ? '/login' : clawiz.config.ui.loginPath;

    if ( clawiz == undefined ) {
      console.log('ClawizBuilder.clawiz undefined')
      this.router.navigate([loginPath], { queryParams: { returnUrl: state.url }});
      return false;
    }

    if ( clawiz.node == undefined) {
      console.log('ClawizBuilder.clawiz.node undefined')
      this.router.navigate([loginPath], { queryParams: { returnUrl: state.url }});
      return false;
    }

    console.log('User not logged');
    this.router.navigate([loginPath], { queryParams: { returnUrl: state.url }});
    return false;

  }
}
