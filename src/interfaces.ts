/*
 * Copyright (c) ISOApplication 2017.
 */

import {
    Clawiz, Service, ClawizConfig, ApplicationConfig, StorageConfig, ApiConfig, UiConfig,
    RouterService
} from "clawiz-core";
import {Router, Routes} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {HttpClient} from '@angular/common/http';

export interface AngularApplicationConfig extends ApplicationConfig {
}

export interface AngularStorageConfig extends StorageConfig {
}

export interface AngularApiConfig extends ApiConfig {
}

export interface AngularApiConfig extends ApiConfig {
  http : HttpClient;
}

export interface AngularUiConfig extends UiConfig {

  loginPath?              : string;
  homePath?               : string;
  startingPath?           : string;
  angularRouter?          : Router;
  toastr?                 : ToastrService;

}

export interface AngularClawizConfig extends ClawizConfig {

  application? : AngularApplicationConfig;
  storage?     : AngularStorageConfig;
  api?         : AngularApiConfig;
  ui?          : AngularUiConfig;

}

export interface AngularClawiz extends Clawiz {

  config : AngularClawizConfig;

}

export interface AngularService extends Service {

  clawiz : AngularClawiz;

}

export interface AngularRouterService extends RouterService {

  clawiz : AngularClawiz;

}

