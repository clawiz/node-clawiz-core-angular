/*
 * Copyright (c) Clawiz
 */

import {AbstractService} from "clawiz-core";
import {AngularClawiz, AngularService} from "../interfaces";
export class AngularAbstractService extends AbstractService implements AngularService {

  public get clawiz() : AngularClawiz {
    return <AngularClawiz>this.clawiz;
  }

}
