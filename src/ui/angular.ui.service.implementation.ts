/*
 * Copyright (c) ISOApplication 2017.
 */

import { UiServiceImplementation, Loading, YesNoDialogOptions  } from "clawiz-core";
import {AngularClawiz, AngularClawizConfig, AngularRouterService} from "../interfaces";
import { Router } from '@angular/router';
import { default as swal } from 'sweetalert2';

export class AngularUiServiceImplementation extends UiServiceImplementation {

  private _angularClawiz : AngularClawiz;

  private _angularRouter : AngularRouterService;

  get clawiz() : AngularClawiz {
    return this._angularClawiz;
  }

  set clawiz(clawiz : AngularClawiz ) {
    this._angularClawiz = clawiz;
  }


  get router(): AngularRouterService {
    return this._angularRouter;
  }

  set router(router: AngularRouterService) {
    this._angularRouter = router;
  }

  get config() : AngularClawizConfig {
    return this.clawiz.config;
  }

  get angularRouter() : Router {
    return this.config.ui.angularRouter;
  }

  openHomePage(): Promise<any> {
    return this.angularRouter.navigate([this.config.ui.homePath != null ? this.config.ui.homePath : '/']);
  }

  openLoginPage(): Promise<any> {
    return this.angularRouter.navigate([this.config.ui.loginPath != null ? this.config.ui.loginPath : '/login']);
  }

  showLoading(text?: string): Promise<Loading> {
    return super.showLoading(text);
  }

  showMessageAlert(title: string, text: string, callback?: () => void): Promise<any> {
    return Promise.resolve(this.config.ui.toastr.info(text, title));
  }

  showErrorAlert(message?: string, callback?: () => void): Promise<any> {
    return Promise.resolve(this.config.ui.toastr.error(message, "Ошибка"));
  }

  showYesNoDialog(options : YesNoDialogOptions) : Promise<any> {

    var focusConfirm   = ! options.defaultNo;
    var focusCancel    = options.defaultNo;

    // var focusConfirm   = false;
    // var focusCancel    = false;

    // var reverseButtons = options.defaultNo;
    var reverseButtons = false;

    var confirmButtonClass = 'btn btn-primary btn-outline pr-2 mx-2 clawiz-margin-right-10';
    var cancelButtonClass  = 'btn btn-primary btn-outline pr-2 mx-2 clawiz-margin-right-10';

    var html               = '<span class="clawiz-dialog-text">' + options.message + '</span>';

    return swal({
      // title             : options.title,
      // text              : html,
      // text              : html,
      html               : html,
      // type              : 'question',
      showCancelButton: true,
      confirmButtonText  : 'Да',
      cancelButtonText   : 'Нет',
      animation          :false,
      focusConfirm       : focusConfirm,
      focusCancel        : focusCancel,
      confirmButtonClass : confirmButtonClass,
      cancelButtonClass  : cancelButtonClass,
      reverseButtons     : reverseButtons,
      buttonsStyling     : false
    }).then((result) => {

      if ( result ) {
        options.yesCallback();
      } else {
        options.noCallback();
      }

      return Promise.resolve();

    });
  }

}
