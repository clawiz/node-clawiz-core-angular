/*
 * Copyright (c) ISOApplication 2017.
 */

import { Component }       from '@angular/core';
import {AngularClawiz} from "../../interfaces";
import {AngularClawizBuilder} from "../../builder/angular.clawiz.builder";
import {AbstractUiComponent} from "clawiz-core";
import {Location} from "@angular/common";
import {ActivatedRoute, Router} from "@angular/router";
@Component({
})
export class AbstractAngularUiComponent extends AbstractUiComponent {

  private _router         : Router;
  private _activatedRoute : ActivatedRoute;
  private _location       : Location;

  constructor(router : Router, activatedRoute : ActivatedRoute, location : Location)  {
    super();
    this.clawiz = AngularClawizBuilder.clawiz;
    this._activatedRoute    = activatedRoute;
    this._location          = location;
    this._router            = router;
    this.init();
  }

  get clawiz(): AngularClawiz {
    return this.clawiz;
  }

  set clawiz(value: AngularClawiz) {
    this.clawiz = value;
  }

  init(): Promise<any> {
    this.prepareDataSourceFields();
    return super.init();
  }

  public ngOnInit() : any {

    this.beforeOpen();

  }


}

