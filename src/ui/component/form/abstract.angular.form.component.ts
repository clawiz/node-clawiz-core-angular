/*
 * Copyright (c) ISOApplication 2017.
 */

import {AbstractFormComponent, FormModel, QuerySelectValuesResponse} from "clawiz-core";
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ElementRef } from '@angular/core';
import {el} from "@angular/platform-browser/testing/src/browser_util";

declare var $: any;

export class AbstractAngularFormComponent<M extends FormModel> extends AbstractFormComponent<M> {

  private parametersSubscribe : any;

  private _router         : Router;
  private _activatedRoute : ActivatedRoute;
  private _location       : Location;

  constructor(router : Router, activatedRoute : ActivatedRoute, location : Location)  {
    super();
    this._activatedRoute    = activatedRoute;
    this._location          = location;
    this._router            = router;
  }


  get router(): Router {
    return this._router;
  }

  get activatedRoute(): ActivatedRoute {
    return this._activatedRoute;
  }

  get location(): Location {
    return this._location;
  }


  public ngOnInit() : any {

    this.parentLinkValues = null;

    this.parametersSubscribe = this.activatedRoute.params.subscribe(params => {

      this.action    = params['action'];
      let parameters = params['parameters'] != null ? JSON.parse(params['parameters']) : null;

      if ( this.action == 'edit' ) {
        this.id               = parameters.id;
      } else if ( this.action == 'create') {
        this.parentLinkValues = parameters;
      }

      this.beforeOpen();
    });

  }

  ngOnDestroy() {
    this.parametersSubscribe.unsubscribe();
  }


  afterSaveClose(): Promise<any> {
    this.location.back();
    return Promise.resolve();
  }

  afterDiscardClose(): Promise<any> {
    this.location.back();
    return Promise.resolve();
  }

  protected prepareDateInputElement(element : ElementRef): Promise<any> {

    if (element == null ) {
      return Promise.resolve();
    }

    let id   : string = element.nativeElement.id;
    let name : string = element.nativeElement.name;

    $('#' + id).datepicker(
      {
        autoclose : true,
        language  : this.clawiz.config.locale
      })
      .on('changeDate', (e: any) => {
        this.data[name] = e.date;
      });

    return super.prepareDateInput({
      id   : id,
      name : name
    })
  }

  protected prepareStaticSelectInputElement(element : ElementRef) : Promise<any> {

    if (element == null ) {
      return Promise.resolve();
    }

    let id     : string = element.nativeElement.id;
    let name   : string = element.nativeElement.name;

    return super.prepareStaticSelectInput({
      id      : id,
      name    : name,
      options : this.data[name + 'SelectOptions']
    });
  }


  public processTypeaheadQuery(name : string, pattern : any, callback : any) {

    this.querySelectValues({
      name    : name,
      pattern : pattern
    })
      .then((response : QuerySelectValuesResponse) => {
        let values : any[] = [];

        if ( response.success ) {
          for ( let value of response.values ) {
            values.push({
              id   : value.value,
              name : value.text
            })
          }
        }

        callback(values);
      });

  }

  protected prepareDynamicSelectInputElement(element : ElementRef) : Promise<any> {

    if (element == null ) {
      return Promise.resolve();
    }

    let id     : string = element.nativeElement.id;
    let name   : string = element.nativeElement.name;

    let queryFunction  = '___' + name + 'Query';
    this[queryFunction] = function(val : any, callback : any) {
      this.processTypeaheadQuery(name, val, callback);
    };

    let selectFunction  = '___' + name + 'Select';
    this[selectFunction] = function(val : any) {
      if ( val ) {
        this.data[name] = val.id;
      }
    };

    $('#' + id)
      .typeahead({
        source : this[queryFunction].bind(this),
        minLength : 0,
        autoSelect : false,
        items : 10,
        matcher : function () {
          return true;
        },
        afterSelect : this[selectFunction].bind(this)
      });

    return super.prepareDynamicSelectInput({
      id      : id,
      name    : name
    });
  }


}
