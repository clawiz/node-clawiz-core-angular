/*
 * Copyright (c) ISOApplication 2017.
 */

import {AbstractGridComponent,GridRowModel} from "clawiz-core";
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

export class AbstractAngularGridComponent<M extends GridRowModel> extends AbstractGridComponent<M> {

  private _router           : Router;
  private _activatedRoute   : ActivatedRoute;
  private _location         : Location;
  private _editFormPath     : string;
  private _ngxLoadingConfig : string = "{ backdropBorderRadius: '14px', backdropBackgroundColour: 'rgba(255,255,255,0.2)', primaryColour: '#c0c0c0', secondaryColour: '#d0d0d0', tertiaryColour: '#e0e0e0' }";

  constructor(router : Router, activatedRoute : ActivatedRoute, location : Location)  {
    super();
    this._activatedRoute    = activatedRoute;
    this._location          = location;
    this._router            = router;
  }



  get activatedRoute(): ActivatedRoute {
    return this._activatedRoute;
  }

  get location(): Location {
    return this._location;
  }

  get router(): Router {
    return this._router;
  }


  get editFormPath(): string {
    if ( this._editFormPath == null && this.editFormName != null ) {
      let tokens = this.editFormName.split("\.");
      let name   = tokens[tokens.length-1];
      this._editFormPath = '/' + name[0].toLocaleLowerCase() + name.substring(1);
    }
    return this._editFormPath;
  }

  set editFormPath(value: string) {
    this._editFormPath = value;
  }


  get ngxLoadingConfig(): string {
    return "{ backdropBorderRadius: '14px', backdropBackgroundColour: 'rgba(255,255,255,0.2)', primaryColour: '#c0c0c0', secondaryColour: '#d0d0d0', tertiaryColour: '#e0e0e0' }";
    // return this._ngxLoadingConfig;
  }

  set ngxLoadingConfig(value: string) {
    this._ngxLoadingConfig = value;
  }

  public ngOnInit() : any {

    this.beforeOpen();

  }

  public ngOnDestroy() : any {

    this.beforeClose();

  }

 openEditView(id: string) {
    this.router.navigate([this.editFormPath, 'edit', JSON.stringify({id : id})]);
  }

  openAddView() {
    this.router.navigate([this.editFormPath, 'create', JSON.stringify(this.createParentParameters)]);
  }
}


