import {AbstractRouterService} from "clawiz-core";
import {AngularClawiz, AngularRouterService} from "../../interfaces";
import {Routes} from "@angular/router";


export class AbstractAngularRouterService extends AbstractRouterService implements AngularRouterService {


  private _angularClawiz : AngularClawiz;

  public set clawiz(value: AngularClawiz) {
    this._angularClawiz = value;
  }

  public get clawiz() : AngularClawiz {
    return this._angularClawiz;
  }


}